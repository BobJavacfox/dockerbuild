#!/usr/bin/env bash
docker rm -f java_test
docker rmi java/test:8.0.0
#git pull https://BobTom@bitbucket.org/BobJavacfox/car_wash_api.git
mvn clean install -Dmaven.test.skip=true
docker-compose -f docker-compose.yml up --build -d
